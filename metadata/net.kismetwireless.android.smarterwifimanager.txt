Categories:System
License:GPLv2
Web Site:http://kismetwireless.net/android-swm/
Source Code:http://kismetwireless.net/gitweb/?p=android-smarterwifi.git;a=summary
Issue Tracker:

Auto Name:Smarter Wi-Fi Manager
Summary:Set WiFi state depending on nearby cell towers
Description:
Manages your device's WiFi connection by automatically learning where you use
networks. WiFi is only enabled when you are in a location you have previously
used WiFi, increasing battery life, security, and privacy.
.

Repo Type:git
Repo:https://www.kismetwireless.net/android-smarterwifi.git

Build:2015.03.56,56
    commit=67a63902c880feccb24f65c867277ac466cc7cc1
    subdir=SmarterWifiManager
    gradle=yes
    prebuild=mkdir -p build/outputs/apk  && \
        cd build && \
        ln -s outputs/apk/ apk

Build:2015.04.59,59
    disable=same build is broken for old version.. looks like change in BS?
    commit=e499a21a53e23a58eff672646a4ad13e5d55fdc2
    subdir=SmarterWifiManager
    gradle=yes
    prebuild=mkdir -p build/outputs/apk  && \
        cd build && \
        ln -s outputs/apk/ apk

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2015.04.59
Current Version Code:59

