AntiFeatures:NonFreeNet
Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/ebaschiera/TripleCamel
Issue Tracker:https://github.com/ebaschiera/TripleCamel/issues

Auto Name:TripleCamel
Summary:Check price history of Amazon products
Description:
Checks for product price history using the [http://camelcamelcamel.com/ CamelCamelCamel]
website. You can also set a notification for when the price gets lower. You
just have to share a product from the Amazon app, and choose Triple Camel as
the sharing app from the list.
.

Repo Type:git
Repo:https://github.com/ebaschiera/TripleCamel

Build:1.0.1,2
    commit=v1.0.2
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:2

